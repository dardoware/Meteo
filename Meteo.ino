/************************************************
* Arduino Meteo
*
* Samuel Villafranca
************************************************/

#include <Wire.h>
#include "RTClib.h"
#include "DHT.h"

#define DHTPIN 2
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);
RTC_DS1307 rtc;

float temperatura = 101;
float humedad     = 101;
float tempAparente = 101;


void setup () {
  Serial.begin(57600);
  Wire.begin();
  rtc.begin();
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  dht.begin();
}

void loop () {
  DateTime ahora = rtc.now();
  humedad = dht.readHumidity();
  temperatura = dht.readTemperature();
  if (humedad==101|| temperatura==101) {
    return;
  }
  tempAparente = dht.computeHeatIndex(temperatura, humedad, false);
  Serial.print(ahora.unixtime());
  Serial.print(": ");
  Serial.print("Temp ");
  Serial.print(temperatura);
  Serial.print("ºC ");
  Serial.print(humedad);
  Serial.print("% ");
  Serial.print("Temp aparente ");
  Serial.println(tempAparente);
  Serial.print("ºC ");
  delay(2000);
}



void printTiempo(DateTime t) {
    Serial.print(t.year(), DEC);
    Serial.print('/');
    Serial.print(t.month(), DEC);
    Serial.print('/');
    Serial.print(t.day(), DEC);
    Serial.print(' ');
    Serial.print(t.hour(), DEC);
    Serial.print(':');
    Serial.print(t.minute(), DEC);
    Serial.print(':');
    Serial.print(t.second(), DEC);
    Serial.println();
}
